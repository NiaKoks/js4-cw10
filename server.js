const express = require('express');
const mysql = require('mysql');
const cors = require('cors');
const news = require('./app/news');
const comments = require('./app/comments');

const port = 8000;
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'user',
    password: '15011996',
    database: 'newsSite'
});

app.use('/news',news(connection));
app.use('/comments',comments(connection));

connection.connect((err)=>{
    if (err){
        console.error('error connect:' + err.stack);
        return;
    }
    app.listen(port,()=> {console.log(`Server started on ${port} port`);
    });
   console.log('connected as ID ' + connection.threadId);
});