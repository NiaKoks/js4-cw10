const express = require('express');

const createRouter = connection =>{
  const router = express.Router();
  router.get('/',(req,res)=>{
      connection.query('SELECT * FROM `comments`',(error,results)=>{
        if (error){
            return res.status(500).send({error:"Can't load database"})
        }   return res.send(results);
      });
  router.get('/:id',(req,res)=>{
      connection.query('SELECT * FROM `comments` WHERE `id`=?',req.params.id,(error,results)=>{
        if (error){
            return res.status(500).send({error: "Can't load database"})
        }
        if (results[0]){
            return res.send(results);
        } else return res.status(404).send({error:'Here is no such comment'})
      });
    });
  router.post('/', (req, res) => {
          const comment = req.body;
          connection.query('INSERT INTO `comments` (`news_id`,`comment_author`,`comment_text`) VALUES (?,?,?)',
              [comment.title, comment.description],
              (error,results)=>{
                  return res.status(500).send({error: "Can't load database"})
              });
          return  res.send({message: 'OK'});
      });
  router.delete('/:id',(req, res)=>{
          connection.query('DELETE  FROM `comments` WHERE `id`=?',req.params.id, (error, results)=>{
              if(error){
                  return  res.status(500).send({error:"Can't load database"})
              } else return res.send({message: 'OK'})
          })
      });
  });
 return router;
};
module.exports = createRouter;