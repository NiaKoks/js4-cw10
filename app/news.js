const  express = require('express');
const  nanoid = require('nanoid');
const  multer = require('multer');
const  path = require('path');
const  config = require('../config');

const createRouter = connection =>{
  const router = express.Router();
  const storage = multer.diskStorage({
      destination:(req,file,cb) =>{ cb(null,config.uploadPath);},
      filename: (req,file,cb)=>{cb(null,nanoid()+path.extname(file.originalname))}
  });

  const upload = multer({storage});

  router.get('/',(req,res)=>{
      connection.query('SELECT * FROM `news`',(error,results)=>{
         if (error){
             return res.status(500).send({error:"Can't load database"})
         }  return res.send(results);
      });
    });
  router.get('/news/:id',(req,res)=>{
      connection.query('SELECT * FROM `news` WHERE `id`=?',req.params.id,(error,results)=>{
         if (error){
             return res.status(500).send({error:"Can't load database"})
         }
         if (results[0]){
             return res.send(results);
         } else  return res.status(404).send({error:'Sorry,but here is no such news'})
      });
    });
    router.post('/',upload.single('news_img'),(req,res) =>{
        const news =  req.body;
        if (req.file){
            news.news_img = req.file.filename;
        }connection.query('INSERT INTO `news`(`news_title`,`news_text`,`news_img`,`news_date`) VALUES(?,?,?,?,?)',
            [news.news_title,news.news_text,news.news_img,news.news_date],
            (error,results) =>{
            if (error){ return res.status(500).send({error:"Can't load database"})}
            return res.send({message:"OK"});
        });
    });
  router.delete('/:id',(req,res) =>{
      connection.query('DELETE FROM `news` WHERE `id`=?',req.params.id,(error,results)=>{
          if(error){
              return res.status(500).send({error:"Can't load database"})
          } else return res.send({message:'OK'})
      })
    });
    return router;
};
module.exports = createRouter;