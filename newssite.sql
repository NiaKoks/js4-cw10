CREATE SCHEMA `newsSite` DEFAULT CHARACTER SET utf8;
USE `newsSite`;

CREATE TABLE `news`(
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`news_title` VARCHAR(255) NOT NULL,
`news_text` TEXT NOT NULL,
`news_img` VARCHAR(100),
`news_date` DATE
);
CREATE TABLE `comments`(
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`news_id` INT NULL ,
`comment_author` VARCHAR(255) NULL DEFAULT 'Anonymous' ,
`comment_text` VARCHAR(255) NOT NULL,
INDEX `news_id_fk_idx` (`news_id` ASC),
CONSTRAINT `news_id_fk`
    FOREIGN KEY (`news_id`)
    REFERENCES `news` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

INSERT INTO `news`(`id`,`news_title`,`news_text`,`news_img`,`news_date`)
VALUES
(1,'Breaking news!','you would not believe!',NULL,NOW()),
(2,'Look at this news','just yesterday...',NULL,NOW()),
(3,'Something big is coming!!!','here is the thing...',NULL,NOW());

INSERT INTO `comments`(`id`,`news_id`,`comment_author`,`comment_text`)
VALUES
(1,1,'Marry Lou','OMG,is that what happened?!'),
(2,1,NULL,'no way...!'),
(3,2,'Poppy','i am love it!'),
(4,2,'Kendra','@Poppy, right? It is so cool!'),
(5,3,NULL,'Can not wait!');



